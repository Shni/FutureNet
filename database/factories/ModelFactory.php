<?php


$factory->define(App\Album::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name
    ];
});


$factory->define(App\Photo::class, function (Faker\Generator $faker) {
   $album = App\Album::all()->pluck('id');
    return [
        'album_id' => $album->random(),
        'name' => $faker->name,
        'href' => $faker->url
    ];
});