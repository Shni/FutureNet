<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'AlbumsController@index' );
Route::get('/album/create', 'AlbumsController@create');
Route::post('/album/create', 'AlbumsController@store');
Route::get('/album/{id}', 'PhotosController@index');
Route::get('/album/delete/{id}', 'AlbumsController@destroy');


Route::get('/photo/add/{id}', 'PhotosController@create');
Route::post('/photo/add', 'PhotosController@store');
Route::get('/photo/delete/{id}', 'PhotosController@destroy');


