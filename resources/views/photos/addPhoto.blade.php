@extends('welcome')

@section('content')

    <form method="POST" action="/photo/add">
        {{ csrf_field() }}

        <div class="field">
            <label class="label" for="name">Назва Фотографії</label>
            <p class="control">
                <input class="input" name="name" type="text" placeholder="Введіть назву">
            </p>
        </div>
        <div class="field">
            <label class="label" for="href">Введіть посилання на фотографію</label>
            <p class="control">
                <input class="input" name="href" type="text" placeholder="Введіть посилання">
            </p>
        </div>
        <input type="hidden" name="album_id" value="{{ $id }}">
        <div>
            @include('errors.error')
        </div>
        <button type="submit" class="button is-primary">Submit</button>
    </form>
@endsection