@extends('welcome')

@section('content')
    <h1 class="title">Create</h1>
    <form method="post" action="/album/create">
        {{ csrf_field() }}

        <div class="field">
            <label class="label" for="title">Назва Альбому</label>
            <p class="control">
                <input class="input" name="title" type="text" placeholder="Введіть назву">
            </p>
        </div>
        <div>
            @include('errors.error')
        </div>
        <button type="submit" class="button is-primary">Submit</button>
    </form>

@endsection