@extends('welcome')

@section('content')
    @foreach($photos as $photo)

        <h2 class="subtitle">{{ $photo->name }}</h2>
        <figure class="image is-128x128">
            <img src="{{ $photo->href }}" width="128px" height="128px">
        </figure><br>

        <a href="/photo/delete/{{ $photo->id  }}"> Видалити Фотографію </a>

    @endforeach
@endsection