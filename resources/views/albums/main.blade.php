@extends('welcome')

@section('content')

    <h1 class="title"> Перелік альбомів </h1><hr>
    @foreach($albums as $album)
        <a href="album/{{$album->id}}"> {{ $album->title }}</a>  |
        <a href="album/delete/{{$album->id}}">Видалити альбом</a>  |
        <a href="photo/add/{{ $album->id }}">Додати фотографії</a><br>
    @endforeach
    <hr>
    <a href="/album/create">add album</a>
@endsection