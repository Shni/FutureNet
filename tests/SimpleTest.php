<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SimpleTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function visit_click_and_see()
    {
        $this->visit('/')
            ->click('add album')
            ->see('Create')
            ->seePageIs('/album/create');
    }

    public function seeAlbumName()
    {
        $album  = factory(App\Album::class)->create();

        $this->visit('/')
             ->see($album->title);
    }


    public function created()
    {
        $album  = factory(App\Album::class)->create();
        $photo = factory(App\Photo::class)->create();

        $this->seeInDatabase('photos', ['album_id' => $photo->album_id, 'name' => $photo->name]);
    }
}
